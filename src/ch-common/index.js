let {
  FileError,
  ParsingError
} = require('./errors')

module.exports = exports = {
  FileError,
  ParsingError
}
