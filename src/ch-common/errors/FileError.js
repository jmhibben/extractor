
let CustomVerror = require('custom-verror')

class FileError extends CustomVerror {
  constructor (...args) {
    super(args)
    this.name = FileError
    this.message = this.message || 'an error occured processing a file'
  }
}

exports.FileError = FileError
