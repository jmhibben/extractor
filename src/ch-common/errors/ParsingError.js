
let CustomVerror = require('custom-verror')

class ParsingError extends CustomVerror {
  constructor (...args) {
    super(args)
    this.name = 'ParsingError'
    this.message = this.message || 'unable to construct object'
  }
}

exports.ParsingError = ParsingError
