let { FileError } = require('./FileError')
let { ParsingError } = require('./ParsingError')

module.exports = exports = {
  FileError,
  ParsingError
}
