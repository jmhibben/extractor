/**
 * @file HashJSHasher test
 * @version 0.1.0
 * @author James Hibben
 */
/* eslint-env jasmine */

let { HashJSHasher } = require('../connectors')

// utils
let { createClass } = require('./common')

describe('HashJSHasher', () => {
  beforeAll(() => {
    // eslint-disable-next-line no-undef
    createHasher = createClass(HashJSHasher)
    // eslint-disable-next-line no-undef
    test = (param) => {
      return createHasher.next().value(param) // eslint-disable-line no-undef
    }
  })

  it('doesn\'t need an options object', () => {
    expect(() => {
      test() // eslint-disable-line no-undef
    }).not.toThrow()
  })

  it('hashes string values correctly', () => {
    let hasher = test() // eslint-disable-line no-undef
    let hash = hasher.hash('test string')
    let expected = 'd5579c46dfcc7f18207013e65b44e4cb4e2c2298f4ac457ba8f82743f31e930b'
    expect(hash).toBe(expected)
  })

  it('verifies identical hash strings as matching', () => {
    let hasher = test() // eslint-disable-line no-undef
    let hash = hasher.hash('test string')
    let expected = 'd5579c46dfcc7f18207013e65b44e4cb4e2c2298f4ac457ba8f82743f31e930b'
    expect(hasher.verify(hash, expected)).toBeTruthy()
  })

  it('verifies mismatched hash strings as not matching', () => {
    let hasher = test() // eslint-disable-line no-undef
    let hash = hasher.hash('test strang')
    let expected = 'd5579c46dfcc7f18207013e65b44e4cb4e2c2298f4ac457ba8f82743f31e930b'
    expect(hasher.verify(hash, expected)).toBeFalsy()
  })

  it('throws a TypeError when `_objectHash` is not a string', () => {
    let hasher = test() // eslint-disable-line no-undef
    let hash = 12345
    let expected = 'd5579c46dfcc7f18207013e65b44e4cb4e2c2298f4ac457ba8f82743f31e930b'
    expect(() => {
      return hasher.verify(hash, expected)
    }).toThrowError(TypeError)
  })

  it('throws a TypeError when `_expectedHash` is not a string', () => {
    let hasher = test() // eslint-disable-line no-undef
    let hash = 'd5579c46dfcc7f18207013e65b44e4cb4e2c2298f4ac457ba8f82743f31e930b'
    let expected = 12345
    expect(() => {
      return hasher.verify(hash, expected)
    }).toThrowError(TypeError)
  })
})
