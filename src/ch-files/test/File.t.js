/* eslint-env jasmine */
/**
 * @file File.t.js
 * @version 0.1.0
 * @author James Hibben
 */

let { parse, resolve } = require('path')
let { logger } = require('ch-utils')
let { File } = require('../File')
let { FileError } = require('ch-common')

describe('File class', () => {
  it('throws a ReferenceError if no path is provided to the constructor', () => {
    expect(() => {
      return new File()
    }).toThrowError(ReferenceError)
  })

  it('must be passed a file path to be constructed', () => {
    let path = '../index.js'
    expect(() => {
      return new File(path)
    }).not.toThrow()
  })

  it('can be passed an object path for construction', () => {
    let path = parse('../index.js')
    expect(() => {
      return new File(path)
    }).not.toThrow()
  })

  it('throws a TypeError if something other than a string or object is passed', () => {
    let paths = [
      () => {
        return new File(65536)
      },
      () => {
        return new File(true)
      },
      () => {
        return new File([])
      }
    ]
    for (let path of paths) {
      expect(path).toThrowError(TypeError)
    }
  })

  describe('instances', () => {
    beforeAll(() => {
      /* eslint-disable no-undef */
      path = resolve('src/ch-files/index.js')
    })
    beforeEach(() => {
      file = new File(path)
      /* eslint-enable no-undef */
    })

    it('store the file\'s name', () => {
      expect(file.name).toBe('index') // eslint-disable-line no-undef
    })

    it('retains a copy of the original path', () => {
      expect(file.path).toBe(path) // eslint-disable-line no-undef
    })

    it('cannot change the path, and throws a TypeError when it\'s attempted', () => {
      expect(() => {
        file.path = resolve('../File.js') // eslint-disable-line no-undef
      }).toThrowError(TypeError)
    })

    it('correctly stats an existing file', () => {
      let f = file.stat // eslint-disable-line no-undef
      expect(f.isFile()).toBeTruthy()
    })

    it('throws a FileError when `stat`ing a non-existing file', () => {
      file = new File('../index.js') // eslint-disable-line no-undef
      expect(() => {
        return file.stat // eslint-disable-line no-undef
      }).toThrowError(FileError)
    })
  })
})
