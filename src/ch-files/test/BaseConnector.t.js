/* eslint-env jasmine */

/**
 * @file BaseConnector test
 * @version 0.1.0
 * @author James Hibben
 */

let { BaseConnector } = require('../connectors')
// resources
let { HashJSHasher } = require('../connectors')

describe('BaseConnector', () => {
  it('cannot be constructed without an options object', () => {
    expect(() => {
      return new BaseConnector()
    }).toThrow()
  })

  it('can be passed a Hasher instance', () => {
    expect(() => {
      return new BaseConnector({hasher: new HashJSHasher()})
    }).not.toThrow()
  })

  describe('throws an error when', () => {
    beforeEach(() => {
      bc = new BaseConnector({hasher: new HashJSHasher()}) // eslint-disable-line no-undef
    })

    it('bc.take is called', () => {
      expect(bc.take).toThrow() // eslint-disable-line no-undef
    })

    it('bc.write is called', () => {
      expect(bc.write).toThrow() // eslint-disable-line no-undef
    })

    it('bc.process is called', () => {
      expect(bc.process).toThrow() // eslint-disable-line no-undef
    })

    it('bc.filter is called', () => {
      expect(bc.filter).toThrow() // eslint-disable-line no-undef
    })
  })
})
