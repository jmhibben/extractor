/* eslint-env jasmine */
/**
 * @file BaseHasher test
 * @version 0.1.0
 * @author James Hibben
 */

let { BaseHasher } = require('../connectors')

// utils
let { noop } = require('lodash')
let { createClass } = require('./common')

describe('BaseHasher', () => {
  beforeAll(() => {
    /* eslint-disable no-undef */
    createHasher = createClass(BaseHasher)
    test = (param) => {
      return createHasher.next().value(param)
    }
    /* eslint-enable no-undef */
  })

  it('throws a TypeError when constructed without an options parameter', () => {
    expect(test) // eslint-disable-line no-undef
      .toThrowError(ReferenceError)
  })

  it('throws a ReferenceError if the options parameter is defined, but empty', () => {
    expect(() => {
      return test([]) // eslint-disable-line no-undef
    }).toThrowError(ReferenceError)
  })

  it('throws a ReferenceError when the options parameter is both not an object and not empty', () => {
    expect(() => {
      return test('test') // eslint-disable-line no-undef
    }).toThrowError(TypeError)
  })

  it('throws an error when the hash method is called', () => {
    // this just throws a generic error, so we don't need to check for anything specific
    expect(() => {
      let hash = test({module: 'test', fn: noop}) // eslint-disable-line no-undef
      hash.hash('test string')
    }).toThrow()
  })

  it('throws an error when the verify method is called', () => {
    expect(() => {
      let hash = test({module: 'test', fn: noop}) // eslint-disable-line no-undef
      hash.verify('12345', '12345')
    }).toThrow()
  })
})
