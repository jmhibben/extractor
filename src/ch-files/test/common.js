
function * createClass (_class) {
  while (true) {
    yield (param) => {
      return new _class(param)
    }
  }
}

exports.createClass = createClass
