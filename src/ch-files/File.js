/**
 * @file File
 * @version 0.1.0
 * @author James Hibben
 *
 * This "class" is mostly supposed to be used as a thin wrapper for file data. It should
 *  primarily be consumed by the FileContainer class as a means to set and store information
 *  about the file it represents.
 *
 * NOTE: Check to see how to prevent classes from being used without construction
 */
let { statSync } = require('fs-extra')
let { format, parse, resolve } = require('path')
let { isString, isObject, isArray } = require('lodash')
let { FileError } = require('ch-common')

let { logger } = require('ch-utils')
let moduleName = require('./package.json').name

class File {
  constructor (_path) {
    if (!_path) {
      throw new ReferenceError(`${moduleName} - No path provided to File constructor`)
    } else if ((!isString(_path) && !isObject(_path)) || isArray(_path)) {
      throw new TypeError(`${moduleName} - Path must be a string or ParsedPath object`)
    }
    let _p = isObject(_path) ? format(_path) : _path
    let path = resolve(_p)
    this._path = parse(path)
    this._stat = null
    this._name = this._path.name
    logger.log('debug', `File name: ${this.name}`)
  }

  get name () {
    return this._name
  }

  get path () {
    return format(this._path)
  }

  set path (_path) {
    // throw a fucking ReferenceError
    throw new TypeError('File.path is a read-only property')
  }

  get stat () {
    logger.log('verbose', 'Checking if file has already been `stat`ed...')
    try {
      /* istanbul ignore else */
      if (this._stat == null) {
        logger.log('debug', '`stat`ing file ', this.path, '...')
        this._stat = statSync(this.path)
      }
    } catch (err) {
      throw new FileError('unable to `stat` file', {
        cause: err,
        info: {
          file: this.path
        }
      })
    }
    return this._stat
  }
}

exports.File = File
