let hash = require('hash.js')
let { isEqual, isString } = require('lodash')
let { BaseHasher } = require('./BaseHasher')

class HashJSHasher extends BaseHasher {
  constructor () {
    super({module: hash, fn: hash.sha256})
  }

  /**
   * Get the hash from a given object
   * @param {string} _input The string to hash
   * @returns string checksum
   */
  hash (_input) {
    return this._fn().update(_input).digest(this._digest)
  }

  /**
   * Verify that the object has the expected checksum
   * @param {string} _objectHash The file/string/object to verify the checksum for
   * @param {string} _expectedHash The checksum to match
   * @throws TypeError if either _objectHash or _expectedHash are not strings
   */
  verify (_objectHash, _expectedHash) {
    if (!isString(_objectHash)) throw new TypeError('`_objectHash` must be a hash string')
    if (!isString(_expectedHash)) throw new TypeError('`_expectedHash` must be a hash string')
    return isEqual(_objectHash, _expectedHash)
  }
}

exports.HashJSHasher = HashJSHasher
