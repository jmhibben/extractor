/**
 * @file BaseConnector
 * @version 0.1.0
 * @author James Hibben
 *
 * The idea for connectors is that they shouldn't care about what data they're passed --
 *  only that they can process them as expected. Requirements:
 * - Data that is invalid or malformed should raise either an error or warning (it should
 *   _only_ throw an error when the problem simply cannot be recovered from).
 * - Functions should return a reference to the Connector instance for method chaining
 *   (exception: DatabaseConnector; this should only require a single function call to `write`).
 * - The final return should directly written to the database through a `save` function
 *   that accepts a DatabaseConnector instance, and will throw an error if one is not
 *   provided (need to consider communication between connectors to avoid this in future versions).
 * - The required API should be defined in BaseConnector and throw errors if they're not defined
 *   by inheriting classes.
 */

class BaseConnector {
  /**
   * Defines the interface used for connecting source and target globs
   * @param {Object} _opts Options to pass to the connector
   * @param {Object} _opts.hasher the hasher to use; default `HashJSHasher`
   */
  constructor (_opts) {
    this.files = []
    this.hasher = _opts.hasher
  }

  /**
   * Takes a set of values and parses to a flattened array. Must be defined by inheriting classes.
   *  Flattens its argument(s) to a one-dimensional array.
   * @param {Array} _data can also be a comma-separated list of values, including other arrays
   */
  take (..._data) {
    throw new ReferenceError('Method `take` must be defined by an inheriting class.')
  }

  /**
   * Writes the final array contents to the provided DatabaseConnector.
   * @param {DatabaseConnector} _dbConnector Database to write to
   */
  write (_dbConnector) {
    throw new ReferenceError('Method `write` must be defined by an inheriting class.')
  }

  /**
   * Process the data before writing. `_processor` must be a function that accepts a single
   *  parameter and returns the processed data.
   * @param {Function} _processor The data to be parsed and processed as necessary
   */
  process (_processor) {
    throw new ReferenceError('Method `process` must be defined by an inheriting class.')
  }

  /**
   * Filter the input using `_filter`. Filter must be either a RegExp or a function
   *  returning a boolean value.
   * @param {Function|RegExp} _filter describes how to filter the data
   */
  filter (_filter) {
    throw new ReferenceError('Method `filter` must be defined by an inheriting class.')
  }
}

exports.BaseConnector = BaseConnector
