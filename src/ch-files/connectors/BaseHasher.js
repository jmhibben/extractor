/**
 * @file Hasher
 * @version 0.1.0
 * @author James Hibben
 */

let { isArray, isEmpty, isObject } = require('lodash')

class BaseHasher {
  constructor (_opts) {
    if (isEmpty(_opts)) throw ReferenceError('no options passed to Hasher')
    if (!isObject(_opts) || isArray(_opts)) throw TypeError('options must be an object')
    this._module = _opts.module
    this._fn = _opts.fn
    this._digest = 'hex'
  }

  hash (_object) {
    throw Error('hash function needs to be implemented')
  }

  verify (_objectHash, _expectedHash) {
    throw Error('verify function needs to be implemented')
  }
}

exports.BaseHasher = BaseHasher
