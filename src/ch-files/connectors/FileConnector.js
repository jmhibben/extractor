/**
 * @file File Connector
 * @version 0.1.0
 * @author James Hibben
 */
/* istanbul ignore-file */

let { cloneDeep, flatten } = require('lodash')
let { ParsingError } = require('ch-common')

let { BaseConnector } = require('./BaseConnector')
let { File } = require('../File')
let { logger } = require('ch-utils')

class FileConnector extends BaseConnector {
  take (..._files) {
    let self = this
    logger.log('verbose', 'Flattening argument(s)...')
    let files = flatten(_files)
    logger.log('verbose', 'Adding files to internal file list...')
    for (let file of files) {
      logger.log('debug', `${file}`)
      self.add(file)
    }
    logger.log('verbose', 'Files added. Moving on...')
    return self
  }

  write (_dbConnector) {
    logger.log('verbose', 'Writing files to database...')
    while (this.files.length > 0) {
      let file = this.files.shift()
      logger.log('debug', `${file.name}`)
      _dbConnector.write(file)
    }
    logger.log('verbose', 'Done.')
  }

  process (_fn) {
    let self = this
    logger.log('verbose', 'Cloning internal file list...')
    logger.log('debug', 'Files: ', self.files)
    let list = cloneDeep(self.files)
    logger.log('verbose', 'Cloning complete.\nResetting internal file list..')
    self.files = []
    logger.log('verbose', 'Processing files...')
    for (let _file of list) {
      let file
      logger.log('debug', `Attempting to construct file ${_file}`)
      try {
        file = new File(_file)
        logger.log('debug', 'Construction successful.')
        logger.log('debug', `Adding file object ${file.name} back to internal file list...`)
        self.files.push(file)
        logger.log('debug', 'File added.')
      } catch (_err) {
        let err = new ParsingError('unable to construct File object', _err, {
          info: {
            file: _file
          }
        })
        logger.log('error', `Construction of file failed: `, err)
        throw err
      }
    }
    logger.log('verbose', 'Processing complete. Moving on...')
    return self
  }

  filter (_fn) {
    let self = this
    logger.log('verbose', 'Cloning internal file list...')
    let list = cloneDeep(self.files)
    logger.log('verbose', 'Cloning complete.\nResetting internal file list..')
    self.files = []
    logger.log('verbose', 'Filtering files...')
    for (let file of list) {
      logger.log('debug', `Testing file ${file}`)
      let accepted = _fn(file)
      logger.log('debug', `Filter result on ${file.name || file}: ${accepted ? 'accept' : 'reject'}`)
      if (accepted) {
        logger.log('debug', `Adding ${file.name || file} back to internal file list...`)
        self.files.push(file)
      }
    }
    logger.log('verbose', 'Filtering complete. Moving on...')
    return self
  }
}

exports.FileConnector = FileConnector
