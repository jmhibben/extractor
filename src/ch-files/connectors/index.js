/**
 * @file connectors export file
 * @version 0.1.0
 * @author James Hibben
 */

// Connectors
let {BaseConnector} = require('./BaseConnector')
// (file) Hashers
let {BaseHasher} = require('./BaseHasher')
let {HashJSHasher} = require('./HashJSHasher')

module.exports = exports = {
  BaseConnector,
  BaseHasher,
  HashJSHasher
}
