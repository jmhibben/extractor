/**
 * @file isValidPath
 * @version 0.2.0
 * @author James Hibben
 */

// local imports
let { _isPathString } = require('./isPathString')
let { _isPathObject } = require('./isPathObject')
let { _isConfigFile } = require('./isConfigFile')

/**
 * Test if the provided path is valid and points to a config file.
 * @param {string|Object} _path The object or string path to test
 * @returns `true` if it is both valid and a config file; `false` otherwise
 */
async function isValidConfig (_logger, _path) {
  _logger.log('debug', 'Checking for _path argument')
  if (!_path) {
    _logger.log('debug', 'No _path provided; not a valid config')
    return false
  }
  _logger.log('debug', 'Verifying that path exists...')
  let isValid = await _isPathString(_logger, _path) || await _isPathObject(_logger, _path)
  _logger.log('debug', 'Checking if filename is a config file...')
  let isConfig = await _isConfigFile(_logger, _path)
  _logger.log('debug', `\tisValid: ${isValid}\n\tisConfig: ${isConfig}`)
  return isConfig && isValid
}

exports.isValidConfig = isValidConfig
