/* eslint-env jasmine */
/**
 * @file loadConfig.t (consider renaming)
 * @version 0.2.0 (this should match the version for the file it's testing)
 */
let {
  loadConfig
  , logger // uncomment when needed
} = require('../')
let { isFunction } = require('lodash')
let { stat } = require('fs-extra')
let { parse } = require('path')

// must be synchronous
describe('`loadConfig`', () => {
  beforeAll(() => {
    // eslint-disable-next-line no-undef
    expected = {
      db: {
        location: '~/.local/share/sync-extractor/filesdb.json',
        saveBackups: false
      }
    }
  })

  it('is an async function', () => {
    let AsyncFunction = Object.getPrototypeOf(async function () {})
    expect(isFunction(loadConfig)).toBe(true)
    expect(Object.getPrototypeOf(loadConfig).constructor === AsyncFunction)
  })

  describe('returns the default config when', () => {
    it('no path is passed', async () => {
      let c = await loadConfig(logger)
      expect(c).toEqual(expected) // eslint-disable-line no-undef
    })

    it('a nonexistent path is passed', async () => {
      let c = await loadConfig(logger, '/path/to/nonexistent/file')
      expect(c).toEqual(expected) // eslint-disable-line no-undef
    })

    it('a directory is passed', async () => {
      let c = await loadConfig(logger, './src')
      let obj = await stat('./src')

      expect(obj.isDirectory()).toBeTruthy()
      expect(c).toEqual(expected) // eslint-disable-line no-undef
    })

    it('a non-config file is passed', async () => {
      let c = await loadConfig(logger, './src/ch-utils/index.js')
      expect(c).toEqual(expected) // eslint-disable-line no-undef
    })
  })

  describe('returns a user config', () => {
    beforeAll(() => {
      // eslint-disable-next-line no-undef
      expected = {
        db: {
          location: '~/.local/share/sync-extractor/filesdb.json',
          saveBackups: false
        },
        test: 'config file'
      }
    })

    it('when a valid config path string is passed', async () => {
      let c = await loadConfig(logger, './src/ch-utils/test/mocks/test.config.js')
      expect(c).toEqual(expected) // eslint-disable-line no-undef
    })

    it('when a valid config path object is passed', async () => {
      let p = parse('./src/ch-utils/test/mocks/test.config.js')
      let c = await loadConfig(logger, p)
      expect(c).toEqual(expected) // eslint-disable-line no-undef
    })
  })
})
