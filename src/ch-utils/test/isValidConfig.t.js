/* eslint-env jasmine */
/**
 * @file isValidPath.t (consider renaming)
 * @version 0.2.0 (this should match the version for the file it's testing)
 * @author James Hibben
 */
// module to test
let {
  isValidConfig
  , logger // uncomment when needed
} = require('../.')
// resources
let { parse } = require('path')
// let { stat } = require('fs-extra')

describe('isValidConfig', () => {
  describe('returns false', () => {
    beforeEach(() => {
      // eslint-disable-next-line no-undef
      basePath = './src/ch-utils/test/mocks/'
    })

    it('if no path is passed', async () => {
      let isValid = await isValidConfig(logger)
      expect(isValid).toBeFalsy()
    })

    it('if a bad path string is passed', async () => {
      // eslint-disable-next-line no-undef
      let isValid = await isValidConfig(logger, `${basePath}test.confi.js`)
      expect(isValid).toBeFalsy()
    })

    it('if an empty object is passed', async () => {
      let isValid = await isValidConfig(logger, {})
      expect(isValid).toBeFalsy()
    })

    it('if an invalid path object is passed', async () => {
      let isValid1 = await isValidConfig(logger, {
        dir: basePath, // eslint-disable-line no-undef
        name: 'test.c',
        ext: '.js'
      })
      let isValid2 = await isValidConfig(logger, {
        what: 'should',
        a: 'path',
        object: {look: 'like'}
      })
      expect(isValid1).toBeFalsy()
      expect(isValid2).toBeFalsy()
    })

    it('if a non-config file is passed', async () => {
      let isValid = await isValidConfig(logger, {
        dir: basePath, // eslint-disable-line no-undef
        base: 'test.js'
      })
      expect(isValid).toBeFalsy()
    })
  })

  describe('returns true', () => {
    beforeAll(() => {
      // eslint-disable-next-line no-undef
      basePath = './src/ch-utils/test/mocks/test.config.js'
    })

    it('when a valid config path string is passed', async () => {
      // eslint-disable-next-line no-undef
      let isValid = await isValidConfig(logger, basePath)
      expect(isValid).toBeTruthy()
    })

    it('when a valid config path object is passed', async () => {
      let pathObj = parse(basePath) // eslint-disable-line no-undef
      let isValid = await isValidConfig(logger, pathObj)
      expect(isValid).toBeTruthy()
    })
  })
})
