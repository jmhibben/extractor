/* eslint-env jasmine */
/**
 * @file isConfigFile.t
 * @version 0.2.0
 * @author James Hibben
 */

let { _isConfigFile } = require('../isConfigFile')
// resources
let { parse, resolve } = require('path')
let { logger } = require('../.')

describe('_isConfigFile', () => {
  describe('returns false', () => {
    it('when the path does not include `conf` or `config`', async () => {
      let _path = resolve('./ch-utils/test/mocks/test.js')
      let isConfig = await _isConfigFile(logger, _path)
      expect(isConfig).toBeFalsy()
    })

    it('regardless if the file is real', async () => {
      let _pathFake = resolve('./ch-utils/test/mocks/test.confi.js')
      let _pathReal = resolve('./ch-utils/test/mocks/test.js')
      let isConfigFake = await _isConfigFile(logger, _pathFake)
      let isConfigReal = await _isConfigFile(logger, _pathReal)
      expect(isConfigFake).toBeFalsy()
      expect(isConfigReal).toBeFalsy()
    })
  })

  describe('returns true', () => {
    it('when the path includes `conf` or `config`', async () => {
      let _path = resolve('./ch-utils/test/mocks/test.config.js')
      let isConfig = await _isConfigFile(logger, _path)
      expect(isConfig).toBeTruthy()
    })

    it('regardless if the file is real', async () => {
      let _pathFake = resolve('./ch-utils/test/mocks/test.conf.js')
      let _pathReal = resolve('./ch-utils/test/mocks/test.config.js')
      let isConfigFake = await _isConfigFile(logger, _pathFake)
      let isConfigReal = await _isConfigFile(logger, _pathReal)
      expect(isConfigFake).toBeTruthy()
      expect(isConfigReal).toBeTruthy()
    })

    it('if the path provided is a parsed path object', async () => {
      let _pathObj = parse('./ch-utils/test/mocks/test.config.js')
      let isConfig = await _isConfigFile(logger, _pathObj)
      expect(isConfig).toBeTruthy()
    })
  })
})
