/* eslint-env jasmine */
/**
 * @file isPathString.t
 * @version 0.2.0
 * @author James Hibben
 */

let { _isPathString } = require('../isPathString')
// resources
let { parse, resolve } = require('path')
let { logger } = require('../.')

describe('_isPathString', () => {
  describe('returns false', () => {
    it('when nothing is passed', async () => {
      let isValid = await _isPathString(logger)
      expect(isValid).toBeFalsy()
    })

    it('when a non-string is passed', async () => {
      let cases = [
        12345,
        ['one', 'two', 'three', 'four', 'five'],
        parse('./package.json')
      ]

      let results = []
      for (let i = 0; i < 3; ++i) {
        let res = await _isPathString(logger, cases[i])
        results.push(res)
      }
      expect(results[0]).toBeFalsy()
      expect(results[1]).toBeFalsy()
      expect(results[2]).toBeFalsy()
    })
  })

  describe('returns true', () => {
    it('when a relative path to a file is passed', async () => {
      let p = './package.json'
      let isPath = await _isPathString(logger, p)
      expect(isPath).toBeTruthy()
    })

    it('when a relative path to a folder is passed', async () => {
      let p = '.'
      let isPath = await _isPathString(logger, p)
      expect(isPath).toBeTruthy()
    })

    it('when an absolute path is passed', async () => {
      let p = resolve('./package.json')
      let isPath = await _isPathString(logger, p)
      expect(isPath).toBeTruthy()
    })
  })
})
