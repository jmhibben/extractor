/* eslint-env jasmine */
/**
 * @file isPathObject.t
 * @version 0.2.0
 * @author James Hibben
 */
let { _isPathObject } = require('../isPathObject')
// resources
let { parse, resolve, sep } = require('path')
let { logger } = require('../.')

// logger.level = 'debug'

describe('_isPathObject', () => {
  describe('returns false', () => {
    it('when nothing is passed', async () => {
      let isPath = await _isPathObject(logger)
      expect(isPath).toBeFalsy()
    })

    it('when a string is passed', async () => {
      let isPath = await _isPathObject(logger, './package.json')
      expect(isPath).toBeFalsy()
    })

    it('when an empty object is passed', async () => {
      let isPath = await _isPathObject(logger, {})
      expect(isPath).toBeFalsy()
    })

    it('when the object provided does not have the correct properties', async () => {
      let isPath = await _isPathObject(logger, {test: 'prop', should: 'fail'})
      expect(isPath).toBeFalsy()
    })

    it('when the object does not represent a complete path', async () => {
      let isPath1 = await _isPathObject(logger, {root: '.', name: 'package'})
      let isPath2 = await _isPathObject(logger, {dir: '.', ext: '.json'})
      let isPath3 = await _isPathObject(logger, {base: 'package.jso', name: 'package'})
      expect(isPath1).toBeFalsy()
      expect(isPath2).toBeFalsy()
      expect(isPath3).toBeFalsy()
    })
  })

  describe('returns true', () => {
    beforeAll(() => {
      /* eslint-disable no-undef */
      resolvedPath = resolve('.')
      resolvedPath += sep
      /* eslint-enable no-undef */
    })

    it('when a full path object is provided', async () => {
      // returning an absolute path
      // as weird as this may look, it works because resolve returns an absolute path;
      //  this is important here, as it guarantees that
      //    a) it points to the parent project's package.json, and
      //    b) all fields in the parsed path object are used
      let parsed = parse(resolve('./package.json'))
      let isPath = await _isPathObject(logger, parsed)
      expect(isPath).toBeTruthy()
    })

    it('when `dir` and `base` are provided', async () => {
      let isPath = await _isPathObject(logger, {dir: '.', base: 'package.json'})
      expect(isPath).toBeTruthy()
    })

    it('when `dir`, `name`, and `ext` are provided', async () => {
      let isPath = await _isPathObject(logger, {dir: '.', name: 'package', ext: '.json'})
      expect(isPath).toBeTruthy()
    })

    it('when `root` and `base` are provided', async () => {
      // eslint-disable-next-line no-undef
      let isPath = await _isPathObject(logger, {root: resolvedPath, base: 'package.json'})
      expect(isPath).toBeTruthy()
    })

    it('when `root`, `name`, and `ext` are provided', async () => {
      // eslint-disable-next-line no-undef
      let isPath = await _isPathObject(logger, {root: resolvedPath, name: 'package', ext: '.json'})
      expect(isPath).toBeTruthy()
    })

    it('when only `root` is provided', async () => {
      // eslint-disable-next-line no-undef
      let isPath = await _isPathObject(logger, {root: resolvedPath})
      expect(isPath).toBeTruthy()
    })

    it('when only `dir` is provided', async () => {
      // eslint-disable-next-line no-undef
      let isPath = await _isPathObject(logger, {dir: resolvedPath})
      expect(isPath).toBeTruthy()
    })
  })
})
