/**
 * @file isPathObject
 * @version 0.2.0
 * @author James Hibben
 */
let fs = require('fs-extra')
let { format } = require('path')
let { isEmpty, isObject } = require('lodash')

function _dirOrRoot (_path) {
  let valid = _path.hasOwnProperty('dir') || _path.hasOwnProperty('root')
  return valid
}

function _baseOrName (_path) {
  let valid = _path.hasOwnProperty('base')
  valid = valid || (_path.hasOwnProperty('name') && _path.hasOwnProperty('ext'))
  return valid
}

function _validateParsedPath (_path) {
  let valid = _dirOrRoot(_path)
  valid = (valid && _baseOrName(_path)) || valid
  return valid
}

/**
 * Test if the object is a valid ParsedPath object
 * @param {Object} _path The path to test
 * @returns `true` if it is a valid ParsedPath object (ie, points to a file or directory); `false` otherwise
 */
async function _isPathObject (_logger, _path) {
  let valid = true

  _logger.log('verbose', 'Checking _path argument...')
  if (_path == null || isEmpty(_path) || !isObject(_path)) {
    _logger.log('debug', 'No _path argument was supplied, or _path is not an object')
    valid = false
  }

  if (valid) {
    try {
      _logger.log('verbose', 'Validating ParsedPath object...')
      valid = _validateParsedPath(_path)
      _logger.log('debug', `valid: ${valid}`)
      _logger.log('debug', 'Formatting _path...')
      let _p = format(_path)
      _logger.log('debug', '`stat`ing _path...')
      let p = await fs.stat(_p)
      _logger.log('debug', 'validating that _path is a file or directory')
      valid = p.isFile() || p.isDirectory()
    } catch (err) {
      _logger.log('debug', '_path supplied, but does not point to a file or directory')
      valid = false
    }
  }
  _logger.log('verbose', `_path is valid: ${valid}`)
  return valid
}

exports._isPathObject = _isPathObject
