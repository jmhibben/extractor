/**
 * @file index
 * @version 0.1.0
 *
 * Module export file for `ch-utils`. Module version number reflects highest version
 *  number among all exported items.
 */

let { isValidConfig } = require('./isValidConfig')
let { loadConfig } = require('./loadConfig')
let { logger } = require('./logger')

module.exports = exports = {
  isValidConfig,
  loadConfig,
  logger
}
