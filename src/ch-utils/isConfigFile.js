/**
 * @file isPathObject
 * @version 0.2.0
 * @author James Hibben
 */

let { isString } = require('lodash')
let { format } = require('path')

/**
 * Determines if the file is a config file. Assumes that `conf` or `config` is in the filename.
 * @param {string|Object} _path Path to the config file in question
 * @returns {boolean} `true` when the filename contains `.conf` or `.config`; `false` otherwise
 */
async function _isConfigFile (_logger, _path) {
  let p = isString(_path) ? _path : format(_path)
  let regex = /\.conf(ig)?\./i
  let isConfig = regex.test(p)
  return isConfig
}

exports._isConfigFile = _isConfigFile
