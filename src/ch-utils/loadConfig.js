/**
 * @file loadConfig
 * @version 0.2.0
 * @author James Hibben
 */

// global deps
let { assign, cloneDeep, isObject } = require('lodash')
let { format } = require('path')
let explorer = require('cosmiconfig')('sync-extractor')
// local deps
let { isValidConfig } = require('./isValidConfig')

const __defaultConfig = {
  db: {
    location: '~/.local/share/sync-extractor/filesdb.json',
    saveBackups: false
  }
}

/**
 * Load the config from the given file, or the default. Merges the default config with the user config, if one is found.
 * @param {string|Object} _fname Path to the config data; can be either a string or path.parse object
 * @returns Object containing the property `config`; also contains `error` and `log`, if anything unusual happened
 */
let loadConfig = async (_logger, _fname) => {
  _logger.log('verbose', `\nAttempting to load config file ${_fname}...`)
  if (!_fname) {
    return cloneDeep(__defaultConfig)
  }
  let config
  try {
    if (isObject(_fname)) {
      _fname = format(_fname)
    }
    _logger.log('verbose', 'Validating config file...')
    let exists = await isValidConfig(_logger, _fname)
    _logger.log('verbose', `\nConfig file exists? ${exists}`)
    let conf = exists ? await explorer.load(_fname) : {config: {}}
    _logger.log('debug', '\nloadConfig - conf:', conf)
    config = conf.config

  } catch (err) {
    // Theoretically, the function should never get here;
    //  most errors should be guaraded against.
    /* istanbul ignore next */
    _logger.log('debug', '\nloadConfig - Error:', err)
    /* istanbul ignore next */
    config = {}
  }
  // now, merge in defaultConfig and return
  assign(config, __defaultConfig)
  return config
}

exports.loadConfig = loadConfig
