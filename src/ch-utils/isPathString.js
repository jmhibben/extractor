/**
 * @file isPathString
 * @version 0.2.0
 * @author James Hibben
 */
let fs = require('fs-extra')
let { isEmpty, isString } = require('lodash')

/**
 * Test if the provided argument is a string path
 * @param {string} _path The path to test
 * @returns `true` if it is a valid string path (ie, points to a file or directory); `false` otherwise
 */
async function _isPathString (_logger, _path) {
  let valid = true
  _logger.log('debug', 'Checking _path argument...')
  if (isEmpty(_path) || !isString(_path)) {
    valid = false
    _logger.log('debug', '_path argument not supplied')
  }
  _logger.log('debug', '_path argument supplied')

  if (valid) {
    try {
      _logger.log('debug', '`stat`ing _path...')
      let stat = await fs.stat(_path)
      _logger.log('debug', 'validating that _path is a file or directory')
      valid = stat.isFile() || stat.isDirectory()
    } catch (err) {
      _logger.log('debug', '_path supplied, but does not point to a file or directory')
      valid = false
    }
  }
  _logger.log('debug', '_path is valid')
  return valid
}

exports._isPathString = _isPathString
