/**
 * @file logger.js
 * @version 1.0.0
 * @author James Hibben
 *
 * Exports a Winston logger. As this is simply exporting an object from an external library,
 *  it also means that it doesn't need testing in this utility library.
 */

const chalk = require('chalk')
const moment = require('moment')
let winston = require('winston')

winston.addColors({
  error: chalk.red,
  warn: chalk.yellow,
  info: chalk.cyan,
  verbose: chalk.green,
  debug: chalk.magenta
})

moment.defaultFormat = 'YY-MM-DD HH:mm:ss.SSSS (ZZ)'

let transportOpts = {
  timestamp: moment.now(),
  format: (options) => {
    return '\n[' + options.timestamp + ']:' + options.colorize(options.level, options.level.toString()) +
           '\n  ' + (options.message ? options.message + '\n' : '\n') +
            options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : ''
  }
}

let opts = {
  level: 'info',
  transports: [
    new (winston.transports.Console)(transportOpts)
  ]
}

let logger = new (winston.Logger)(opts)

exports.logger = logger
