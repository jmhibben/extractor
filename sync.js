let extractor = require('commander')
let cosmo = require('cosmiconfig')
let fs = require('fs-extra')
let lowdb = require('lowdb')
let FileAsync = require('lowdb/adapters/FileAsync')
let id = require('shortid')
let { noop } = require('lodash')
// program files
let logger = require('./js/logger')
let loadConfig = require('./js/loadConfig')
let state = require('./js/state')

// check if the config file exists;
// if it doesn't, make it using default settings
// then, get the config
let config = loadConfig('~/.config/sync-extractor/extractor.config.json', prog)
let sync = state(config)
// create the file adaptor
let adapter = new FileAsync(config.location) // TODO: change this to get it from the state

// create the database and add it to the state via `sync.addDB()`

extractor
  .version('1.0.0')
  .arguments('<selection>')
  .option('-l, --logging-level', 'Set the logging level', /^(none|error|warn|info|verbose|debug)$/i, false)
  .option('-L, --file-log [file]', 'Log to a file instead; if no file is given with this flag, it will log to `./extract.log`')
  .option('-s, --subdirs', 'Check subdirs for files', true)
  .action((selection) => {
    // set the logger's level according to the extractor.loggingLevel flag
    switch (extractor.loggingLevel) {
      case 'none':
        logger = {
          log: noop
        }
        break
      case 'error':
      case 'warn':
      case 'info':
      case 'verbose':
      case 'debug':
        logger.level = extractor.loggingLevel
        break
      default:
        logger.log('error', `Invalid logging level specified: ${extractor.loggingLevel}`)
        return
    }

    if (selection !== 'all' || selection !== 'modified' || selection !== 'new') {
      // log that a selection was not passed
      logger.log('info', 'No selection was passed; assuming `modified`')
      selection = 'modified'
    }
    // use the selection to scan the current directory
  })
