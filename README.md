# `extractor`

## Development

Development has be done using `yarn`, `jasmine`, and `lerna`. If you do not already have yarn, see [yarn](https://yarnpkg.net)'s official website for install instructions. Before you begin testing, clone the repository to your computer:

```
// ssh
git clone git@gitlab.com:jmhibben/extractor.git
// http
git clone https://gitlab.com/jmhibben/extractor.git
```

- To install all dependencies, run `yarn install`
- To test, run `yarn test`
- To link src modules together for proper testing, run `yarn update`

If you intend to clone it into another GitLab repository, GitLab CI will automatically run the tests for you on push.

### Styleguide

This project uses StandardJS. When writing bugfixes, please follow its guidelines to ensure consistency in this project.

### Merge Requests

In order for merge requests to be properly considered, you _must_ thoroughly test your code. If any tests fail, I will request that you fix any failing test cases before I will consider merging.