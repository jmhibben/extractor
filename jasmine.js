let Jasmine = require('jasmine')
// let reporters = require('jasmine-reporters')
let Reporter = require('jasmine-console-reporter')

let jasmine = new Jasmine()

jasmine.addReporter(new Reporter({
  colors: true,
  cleanStack: 1,
  verbosity: 4,
  listStyle: 'indent',
  activity: 'star',
  emoji: false,
  beep: false
}))

jasmine.loadConfig({
  'spec_dir': 'src',
  'spec_files': [
    '**/test/**/*.t.js'
    // 'ch-util/**/*.t.ts'
    // 'ch-files/test/**/*.t.ts'
  ],
  'stopSpecOnExpectationFailure': false,
  'random': false
})

jasmine.execute()
